package quorumsimulator;

public class Result {
	float index, reads, writes;

	public Result(int index, int reads, int writes, int noNodes, int noOps) {
		this.index = (index * 100) / (float) noNodes;
		this.reads = (reads * 100) / (float) noOps;
		this.writes = (writes * 100) / (float) noOps;
	}

	@Override
	public String toString() {
		return "" + index + " " + reads + " " + writes;
	}
}
