package quorumsimulator;

import java.util.ArrayList;
import java.util.Random;

public class DynamoSimulator extends GenericSimulator {

	public DynamoSimulator() {
		this.title = "dynamo";
	}

	@Override
	public int genericPut(DObject dobj, int value) {
		ArrayList<Integer> randomNodes = new ArrayList<Integer>();
		Random r = new Random();

		/* Set the new value of the object */
		dobj.value = value;

		System.out.println("---Put on object " + dobj.index + "---");

		/*
		 * Select a bunch of nodes to perform write on We are sure that at least
		 * one node will provide the correct value of the object (or will have a
		 * copy)
		 */
		for (int i = 0; i < writes; i++) {
			int nodeIndex = r.nextInt(nodesNo);

			while (randomNodes.contains(new Integer(nodeIndex)))
				nodeIndex = r.nextInt(nodesNo);

			randomNodes.add(new Integer(nodeIndex));
		}

		/* Perform put on the previously selected nodes */
		for (Integer nodeIndex : randomNodes)
			nodes.get(nodeIndex.intValue()).putObject(dobj);

		return 1;
	}

	@Override
	public int genericGet(int objId) {
		ArrayList<Integer> randomNodes = new ArrayList<Integer>();
		Random r = new Random();
		DObject instance;
		int count = 0;

		System.out.print(">>>Get on object " + objId + "<<<");

		/*
		 * Select a bunch of nodes to perform read on We are sure that at least
		 * one node will provide the correct value of the object (or will have a
		 * copy)
		 */
		for (int i = 0; i < reads; i++) {
			int nodeIndex = r.nextInt(nodesNo);

			while (randomNodes.contains(new Integer(nodeIndex)))
				nodeIndex = r.nextInt(nodesNo);

			randomNodes.add(new Integer(nodeIndex));
		}

		/* Perform get on the previously selected nodes */
		for (Integer nodeIndex : randomNodes) {
			instance = nodes.get(nodeIndex.intValue()).getObject(objId);
			if (instance != null)
				count++;
		}

		System.out.println(" " + count);

		/* Return 1 if there is at least one valid replica */
		if (count > 0)
			return 1;
		return 0;
	}
}
