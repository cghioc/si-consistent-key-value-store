package quorumsimulator;

public class DObject {
	int index, value;

	public DObject(int index) {
		this.index = index;
	}

	public int put(int value) {
		this.value = value;
		return 1;
	}

	public int get() {
		return value;
	}

	@Override
	public String toString() {
		return Integer.toString(value);
	}
}
