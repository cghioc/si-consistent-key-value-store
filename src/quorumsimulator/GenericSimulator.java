package quorumsimulator;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.Random;

public abstract class GenericSimulator {
	protected static final String OUT_FILE	= "out.dat";
	protected static final int TEST_VALUE	= 13;
	protected static final int REPEATS		= 15;

	protected ArrayList<Node> nodes = new ArrayList<Node>();
	protected ArrayList<DObject> objects = new ArrayList<DObject>();
	protected ArrayList<Result> results = new ArrayList<Result>();

	protected String configFile;
	protected String title;
	protected int nodesNo, objectsNo, reads, writes;

	/**
	 * Reads configuration file
	 */
	protected void readConfigs() {
		String s;
		File file = new File(configFile);

		try {
			BufferedReader in = new BufferedReader(new FileReader(file));
			s = in.readLine();
			nodesNo = Integer.parseInt(s);
			s = in.readLine();
			objectsNo = Integer.parseInt(s);
			s = in.readLine();
			reads = Integer.parseInt(s);
			s = in.readLine();
			writes = Integer.parseInt(s);

			in.close();
		} catch (Exception e) {
			System.out.println("Unable to read from file");
			e.printStackTrace();
		}

		System.out.println("Simulate with " + nodesNo + " nodes " + objectsNo
				+ " objects " + reads + " reads " + writes + " writes");
	}

	/**
	 * Creates system nodes
	 */
	protected void createNodes() {
		for (int i = 0; i < nodesNo; i++)
			nodes.add(new Node());
	}

	protected void enableAllNodes() {
		for (Node node : nodes)
			node.enable();
	}

	/**
	 * Disable active nodes
	 * 
	 * @param count
	 *            the number of nodes to be disabled
	 */
	protected void disableNodes(int count) {
		ArrayList<Integer> uniques = new ArrayList<Integer>();
		Random r = new Random();
		Integer val;

		System.out.print("Disabling nodes: ");
		while (count > 0) {
			val = new Integer(r.nextInt(nodesNo));

			while (uniques.contains(val))
				val = new Integer(r.nextInt(nodesNo));
			System.out.print(val + ", ");
			uniques.add(val);
			count--;

			nodes.get(val).disable();
		}

		System.out.println();
	}

	protected void addObjects() {
		/* Populate with objects */
		for (int i = 0; i < objectsNo; i++) {
			objects.add(new DObject(i));
		}
	}

	/* Create nodes, objects, assign replicas */
	public void initNodes() {
		readConfigs();
		createNodes();
		addObjects();
		enableAllNodes();
	}


	/**
	 * Compute Load Imbalance
	 */
	public void measureImbalance() {
		int p = nodes.size();
		int sum = 0, avg, diff, max_diff = 0;

		/* Compute the average number of operations per node */
		for (Node node : nodes)
			sum += node.balanceCounter;
		avg = sum / p;

		/* Compute the load imbalance */
		for (Node node : nodes) {
			diff = Math.abs(node.balanceCounter - avg);
			if (diff > max_diff)
				max_diff = diff;
		}

		System.out.println("Imbalance " + max_diff + " with average " + avg);
	}

	/**
	 * Run the simulation
	 */
	public void testSystem() {
		int writeCount = 0, readCount = 0;

		for (int i = 1; i < nodesNo - 1; i++) {
			writeCount = readCount = 0;

			/* Repeat the experiment and compute an average */
			for (int j = 0; j < REPEATS; j++) {
				
				disableNodes(i);

				/* Perform put on all the objects */
				for (DObject dobj : objects)
					writeCount += genericPut(dobj, TEST_VALUE);

				/* Perform get on all the objects */
				for (DObject dobj : objects)
					readCount += genericGet(dobj.index);
				
				enableAllNodes();
			}
			writeCount /= REPEATS;
			readCount /= REPEATS;


			System.out.println("Disabled " + i + " Successful writes "
					+ writeCount + " and reads " + readCount + " of "
					+ objectsNo);
			System.out.println();
			System.out.println();

			results.add(new Result(i, readCount, writeCount, nodesNo, objectsNo));
		}

		/* Compute Load Imbalance */
		measureImbalance();

		/* Write results */
		exportResults();
	}

	/* Print results to file */
	public void exportResults() {
		String file = this.title + "_" + OUT_FILE;
		System.out.println("Writing results to file " + file);

		try {
			FileOutputStream fos = new FileOutputStream(new File(file));
			BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(fos));

			for (Result result : results) {
				bw.write(result.toString());
				bw.newLine();
			}

			bw.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Attempt to write a new value on all available replicas
	 * 
	 * @param dobj
	 *            the object to write to
	 * @param value
	 *            the new content of the object
	 * @return 0 the write didn't succeed 1 the write was successful
	 */
	public abstract int genericPut(DObject dobj, int value);

	/**
	 * Attempt to read values from all the available replicas of a given object
	 * 
	 * @param dobj
	 *            the object to be read
	 * @return 0 the read didn't succeed 1 the read was successful
	 */
	public abstract int genericGet(int objId);

}
