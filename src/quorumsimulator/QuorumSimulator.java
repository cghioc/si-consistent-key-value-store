package quorumsimulator;

public class QuorumSimulator {
	public static final String DYNAMO = "dynamo";
	public static final String GRID = "grid";

	public static void main(String[] args) {

		GenericSimulator simulator = null;

		System.out.println("Starting " + args[0].toUpperCase()
				+ " Simulator...");

		if (args.length < 2) {
			System.out
					.println("How to run: program_name <simulator_type> <config_file>");
			System.out.println("<simulator_type> values: dynamo or grid");
			return;
		}

		if (args[0].equals(DYNAMO))
			simulator = new DynamoSimulator();
		else if (args[0].equals(GRID))
			simulator = new GridSimulator();
		else {
			System.err.println("Simulator type not defined. Exiting...");
			System.exit(1);
		}

		simulator.configFile = args[1];
		System.out.println("Config file is " + simulator.configFile);

		/* Build nodes, objects, assign replicas to nodes */
		simulator.initNodes();

		/* Disable a couple of nodes and compute statistics */
		simulator.testSystem();
	}
}
