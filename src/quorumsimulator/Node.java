package quorumsimulator;

import java.util.ArrayList;

public class Node {
	boolean enabled;
	int index;
	int balanceCounter = 0;
	ArrayList<DObject> objects;
	
	public Node() {
		enabled = false;
		objects = new ArrayList<DObject>();
	}
	
	public boolean putObject(DObject dobj) {
		if (!enabled)
			return false;
		objects.add(dobj);
		
		balanceCounter++;
		
		return true;
	}

	public DObject getObject(int index) {
		if (!enabled)
			return null;
		
		balanceCounter++;

		for (DObject dobj : objects)
			if (dobj.index == index)
				return dobj;

		return null;
	}

	public void removeObject(DObject dobj) {
		if (!enabled)
			return;
		objects.remove(dobj);
	}

	public void enable() {
		this.enabled = true;
		objects.clear();
	}

	public void disable() {
		this.enabled = false;
	}
}
