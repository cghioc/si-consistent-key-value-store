package quorumsimulator;

import java.util.Random;

public class GridSimulator extends GenericSimulator {

	public GridSimulator() {
		this.title = "grid";
	}

	/**
	 * Choose a random line, and attempt to write a value on all the elements of
	 * that line. The write will fail if there is a node not active at that
	 * moment.
	 */
	@Override
	public int genericPut(DObject dobj, int value) {
		int lineIdx, nodeIdx;

		Random rand = new Random();
		lineIdx = rand.nextInt(reads);

		for (int column = 0; column < writes; column++) {
			nodeIdx = lineIdx * writes + column;
			if (!nodes.get(nodeIdx).putObject(dobj))
				return 0;
			else
				System.out.println(nodes.get(nodeIdx).objects);
		}

		return 1;
	}

	/**
	 * Choose a random column from which the read will be made. On the selected
	 * column, there must be at least an element that has the version of the
	 * object. If one of the objects from a column is not available, the read
	 * operation from that column will fail.
	 */
	@Override
	public int genericGet(int objId) {
		int columnIdx, nodeIdx;
		Random rand = new Random();
		columnIdx = rand.nextInt(writes);
		boolean hasValue = false;

		/*
		 * Get all elements from the column. If one node from that column will
		 * be inactive, than the read operation will fail
		 */
		for (int line = 0; line < reads; line++) {
			nodeIdx = line * writes + columnIdx;
			if (!nodes.get(nodeIdx).enabled)
				return 0;
			if (nodes.get(nodeIdx).getObject(objId) != null)
				hasValue = true;
		}

		return (hasValue) ? 1 : 0;
	}

}
