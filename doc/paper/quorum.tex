\documentclass[10pt, conference, compsocconf]{IEEEtran}
\usepackage{cite}
\usepackage{booktabs}
\usepackage{listings}
\usepackage{hyperref}
\usepackage{subfig}
\usepackage{amsmath}




\ifCLASSINFOpdf
  \usepackage[pdftex]{graphicx}
  \graphicspath{{img/}}
  \DeclareGraphicsExtensions{.pdf}
\else
  \usepackage[dvips]{graphicx}
  \graphicspath{{img/}}
  \DeclareGraphicsExtensions{.pdf}
\fi


% correct bad hyphenation here
\hyphenation{op-tical net-works semi-conduc-tor}


\begin{document}
\title{Quorum Systems Availability and Load Imbalance: Dynamo and Grid}

% author names and affiliations
\author{\IEEEauthorblockN{Mihaela-Alexandra Martinas, Claudiu Ghioc}
\IEEEauthorblockA{Faculty of AUTOMATIC CONTROL and COMPUTERS\\
University POLITEHNICA of Bucharest\\
Bucharest, Romania\\
Email: martinas.mihaela21@gmail.com, claudiu.ghioc@gmail.com}}


% make the title area
\maketitle


\begin{abstract}
	Voting based on \emph{quorums} represents a basic mechanism to obtain both
replica control in distributed systems and transaction atomicity.
A \emph{quorum system} relies on this type of
voting by designating a collection of nodes to be responsible with a
transaction, thus obtaining consistency in fault-tolerant and
highly-available distributed computing. This paper compares the
availability and load imbalance of two different policies used in quorum
systems: \emph{Dynamo} and \emph{Grid Systems}.
\end{abstract}

\begin{IEEEkeywords}
	quorum systems, availability, reliability, performance, load
	balance
\end{IEEEkeywords}

\IEEEpeerreviewmaketitle



\section{Introduction}


``\emph{Quorum systems} serve as basic tools providing an uniform and reliable way to achieve
coordination between processors in a distributed system.''\cite{load} They have been
used in the study of problems such as mutual exclusion, data replication
protocols, name servers, selective dissemination of information and
distributed access control and signatures. \cite{optimal}

To achieve the consistency of transactional operations and of the
current state of a distributed system, a quorum system must follow the
rules imposed by a quorum policy and also configure the quorums such as
their intersection is not null. These properties ensure that all the
read operations in a large system will return the latest values of a
write operation, even in the case where some nodes are disabled.

The consistency of read and write operation is of utmost importance in
large distributed systems such as Amazon's e-commerce platform which
serves tens of millions of customers at peak time using tens of
thousands of servers located in many data centers around the world.
 Because they want to provide an ``always on'' experience,
the user must be able to add/remove items from the cart anytime. This
type of behaviour is provided by using a service that always allows
read/write operations to its data stores and data is also available
across multiple data centers.\cite{dyn}

This paper studies two quorum policies and compares the availability and
the load imbalance yielded by each one. We make our statements based on
data obtained after building and running a quorum system simulator. The
application simulates a distributed system in several situations 
where a part of the nodes are disabled and ``clients'' keep performing
\emph{put} and \emph{get} operations and collects statistics related to
the two metrics we are interested in. The rest of the paper is
structured as follows. The next section describes the \emph{quorums systems},
mentions previous work and presents the two quorum policies to be
compared. The third section contains details about the quorum system
simulator and how the two policies have been implemented. The fourth
section presents our experimental setup and the results we have obtained from
the simulations. Finally, the last section concludes and presents our
thoughts on which policy is best suited for a certain situation.


\section{Quorum Systems}
\emph{Quorums} have been used in different fields to denote a number, usually a
majority, needed to make a decision. To prevent undesirable
inconsistencies in a voting process, of critical
importance is requiring a majority to reach decisions.
\cite{quorums} 

In distributed computing, quorums represent groups of physical machines
organized in a way to improve the
availability and efficiency of replicated data, forming \emph{quorum
systems}. ``A \emph{quorum system}
for a universe of data servers is a collection of subsets of
servers, each pair of which intersect. Intuitively, each quorum
can operate on behalf of the system, thus increasing its
availability and performance, while the intersection property
guarantees that operations done on distinct quorums preserve
consistency.'' \cite{byz}

The most important property of \emph{quorum systems} is that the
intersection of each group is a non-empty subset. Other notable aspect of quorums,
namely that they are strict subset of a group of nodes, relates to the
goals of higher availability, better load balancing \cite{load} and
fault tolerance in distributed systems. \cite{quorums} The idea of this
concept is that a client which accesses replicated data on a distributed
system does not need to communicate with all the nodes, but only to the
nodes belonging to a quorum. This contributes to a relaxation of the load on
the nodes outside the quorum and enables tolerance of their failures,
achieving higher availability.

There is a significant number of quorum configurations from which one
can choose according to the problem it intends to solve. \cite{quorums}
exposes a couple of policies such as:
\begin{itemize}
	\item \emph{Singleton} - considers a single node;
	\item \emph{Majority} - which builds a quorum with at least n/2
nodes;
	\item \emph{Grid} - described later in this section;
	\item \emph{Finite Projective Planes} - given \textit{q} a prime
		number and \textit{n} the number of nodes, $n = q^{2} + q + 1$,
		every subset has exactly \emph{q + 1} elements, every element
		being contained in exactly \emph{q + 1} subsets, and every two
		subsets intersect in exactly one element;
	\item \emph{B-Grid} - given \emph{n} the number of nodes, \textit{n
 = dhr}, where \textit{d} is the number of columns, \emph{h} is the
 number of bands and \emph{r} the number of rows per band, a group of
 \emph{r} rows forms a band. A \emph{mini-column} is called a number of
 \emph{r} elements in a column, restricted to a band. A quorum consists
 of one \emph{mini-column} in every band and one element from each
 mini-column of one band.
\end{itemize}

This paper studies and compares two of these mentioned
policies.

\subsection{Dynamo Quorum System}
\emph{Dyanmo} was implemented by Amazon to provide a highly available
key-value storage system and help the company ensure an ``always-on''
experience for its users.

The protocol used is similar with the one implemented by quorum systems, being
based on a minimum number of nodes that must participate to an
operation. To be more precise, given a number \emph{N} of nodes,
\emph{R} of these is the minimum number that must participate to a read
operation and \emph{W} of these is the minimum number that must
participate to a write operation.

To yield a \emph{quorum-like} system, R and W must be set such that
\emph{R + W $>$ N}. This signifies that the set of nodes chosen to
validate the read operation has in common at least one node with the set
of nodes chosen to validate the write operation. In other words, the
intersection between the nodes chosen for each operation is a
non-empty set. This approach ensures that when a read is performed, at
least one node will have the latest version of the requested object.


Dynamo uses a variant of consistent hashing to partition the data and
distribute the load across a set of nodes (storage hosts). Beside the
classic approach (the output range of a hash function is treated as a
fixed circular space or ``ring'' and data identified by a key is
assigned to a node by hashing the data item's key to yield its position
in the ring), Dynamo also uses the concept of \emph{virtual nodes} (each
physical node is responsible for several virtual nodes) to better
disperse the load handled by each node. \cite{dyn}

\subsection{Grid Quorum System} \label{subsec:grid-quorum}
The \emph{Grid} quorum policy implies the logical arrangement of nodes in a
\emph{R} x \emph{W} matrix with \emph{N} elements. A read operation is
considered successful when all the nodes from a logical column are
online and able to respond to the request and at least one
node which holds object's value. On the other hand, for a write operation to be
successful all the nodes in a logical row must complete the transaction
and respond the coordinator. The fact that the intersection between the
read and write quorums is only one element (one row and one column in
the matrix) makes this policy more restrictive, but guarantees that there
is at least one node which holds the latest value for the requested object.
\begin{figure}[h]
    \centering
    \includegraphics[width=1.5in]{grid}
    \caption{Grid Quorum Nodes}
    \label{fig:grid}
\end{figure}


\section{Quorum System Simulator}
To be able to study and compare the two quorum policies we had to
simulate how such systems would behave given certain conditions and
consider only the metrics that we are interested in: availability and load
imbalance. Therefore, instead of implementing the whole systems we
created a small Java program which simulates read and write operations
on nodes (each node is merely a Java object), ``disables'' some of the
nodes and computes the rate of successful operations.

\subsection{Architecture}
The main components of a distributed systems are represented as Java
objects (system nodes, data objects, memory locations) and the state of
each component is simulated using simple state variables.

The application contains a class which offers generic methods to
read testing configuration, initialize the environment, create the
specified number of nodes and objects to be stored in the distributed
system, run the actual simulation and export the results in a file. The
two policies are implemented as separate classes, each containing two methods
that define their behaviour as a quorum policy: \emph{put} and
\emph{get}, as shown in Figure \ref{fig:arc}.

\begin{figure}[h]
    \centering
    \includegraphics[width=1.5in]{arc}
    \caption{Overview of Quorum System Simulator}
    \label{fig:arc}
\end{figure}

The simulation consists of a series of put/get operations executed in
different configurations. Given N, the number of nodes, and M the number
of objects stored in the system, the simulator tries to execute put and
get on each object while the number of enabled nodes decreases from N -
1 to 1. The simulator uses either the Dynamo or the Grid version of the
two methods to write/read data, as previously configured in a file and
at the end saves the availability related statistics.

\subsection{Dynamo Simulator}
The \emph{Dynamo Simulator} overwrites the generic \emph{put} and
\emph{get} methods to match the policy described in \cite{dyn}.

The \emph{put} method simply finds \emph{W} enabled nodes on which to
write a test value for the given object, where \emph{W} is specified in
the configuration file. If there are less than \emph{W}
online nodes the write operation fails and the values written on the
enabled nodes are removed. The simulator will collect the result of each
operation and will add it to the final statistics.

The \emph{get} method creates a list with \emph{R} randomly selected
nodes from which it tries to retrieve the object's value.
If a previous \emph{put} operation succeeded, and the
\emph{R} + \emph{W} $>$ \emph{N} relation has been followed when setting
the quorum parameters then at least one node will return the latest
value stored in the system. This statement does not hold if all the
nodes selected by the \emph{get} operation are disabled (this case is
avoided in our simulation, if the \emph{put} operation was successful
and no other node was disabled).


\subsection{Grid Simulator}
To simulate a grid quorum, as input values for the simulator were
considered the total number of nodes, the number of different objects that one
can find in the system and the number of nodes that must respond to the
read and write operations. By multiplying the values for read and write,
the total number of nodes must be obtained. The nodes are logically
divided into a matrix where the reads are the number of rows and the
writes are the number of columns.


When a \emph{write} operation is performed for an object, a random line is
chosen. The write will succeed only if all the nodes from the chosen line are
available. This behaviour ensures that when the read operation is
performed, there will be at least one element on a column that has a
version of the element.

To perform a \emph{read} operation, a random column is selected.
Each node from the selected column must be active and at least one of
them must have a version of the searched object, otherwise the operation
will fail. If the previous write was successful at least one of the
questioned nodes should have the latest version of the object.


\section{Evaluation of Quorum Systems}
The goal of the simulation was to be able to make a comparison between
the behaviours of two types of quorum protocols under similar conditions.
Availability and load imbalance where the main characteristics targeted
for analysis.

From the total number of nodes that composed the system, on each
evaluation phase one more node was marked as ``disabled'', until the
total number of failed nodes reached a predefined value. At each step,
first a write operation was performed followed by a read operation, for
each of the \texttt{M} types of objects.

What is worth mentioning from the beginning is that the condition that
Grid quorum systems must meet is more strict than the one implemented by Dynamo. This is
because the size of the quorum for a grid is $\sqrt{N}$, whereas for a
Dynamo-like implementation is $\frac{N}{2}$.

In the following subsections, there will be a detailed presentation
about how ``failed'' nodes influence the percent of successful
operations.

\subsection{Availability}
Dynamo-like implementations are known for being used to obtain highly
available key-value systems. The question is if a grid implementation
could be used to obtain such a capability?

The availability was translated into a relationship between the percent of
nodes that were ``disabled'' and the percent of successful read/write
operations.

By comparing the graphics of the two protocols, one can observe that
the read operation is still successful for the Dynamo Simulation, even
if 80-90$\%$ percent of the existing nodes had failed. On the other hand,
for the Grid Simulation, read fails when only 30-40$\%$ percent of the
existing nodes are ``inactive''. The trend of the percent of read operations
that end with success is descending for both cases. For Grid, the
percent of disabled nodes influences in a dramatic manner the output of
the operation. For a smaller percent of deactivated nodes than the one
used for Dynamo, the success rate falls to 0$\%$.

Analysing the writes, the plots show a 100$\%$ success rate for Dynamo
simulation, whereas for the Grid simulation, it is decreasing
proportional with the percent of failed nodes. For Grid, the
write percentage falls in correlation with the read percentage and
again, one can observe the tight link between the number of disabled
nodes and the percent of failed operation. It is not necessary a big
number of nodes to be deactivated to make a write or read fail.

\begin{figure}
	\centering
	\subfloat[R = 50 W = 51]{\includegraphics[width=0.5\textwidth]{plots/dyn1.png}}\\
	\subfloat[R = 30 W = 71]{\includegraphics[width=0.5\textwidth]{plots/dyn2.png}}\\
	\subfloat[R = 20 W = 81]{\includegraphics[width=0.5\textwidth]{plots/dyn3.png}}
	\caption{Dynamo-like Quorum}
\end{figure}

\begin{figure}
	\centering
	\subfloat[R = 10 W = 10]{\includegraphics[width=0.5\textwidth]{plots/grid1.png}}\\ 
	\subfloat[R = 4 W = 25]{\includegraphics[width=0.5\textwidth]{plots/grid2.png}}\\
	\subfloat[R = 2 W = 50]{\includegraphics[width=0.5\textwidth]{plots/grid3.png}}
	\caption{Grid Quorum}
\end{figure}

Also, an important aspect to mention for Grid implementation is that
the smaller the number of nodes that must respond to an operation, the
more likely is the operation will fail earlier than the other (see Fig 3 (c)).

As it was expected, the Grid Quorum simulation showed that the
read/write operations fail for a smaller number of ``deactivated'' nodes
than the Dynamo Quorum simulation. The reason is the less
restrictive condition that Dynamo has to meet in order to declare an
operation successful or not.


\subsection{Load Imbalance}
Assuming that the total amount of computation in a parallel code
fragment, \emph{Wt}, is distributed among \emph{p} nodes in such a
way that each node \emph{i}, 0 $<$ i $<$ p, is assigned an amount of
computation equal to \emph{Wi}, then we say that this distribution
exhibits a \emph{load imbalance}, \emph{L}, equal to:

\[ \emph{L}
	  = \dfrac{\dfrac{a}{b}}{c}
	  = max(Wi - \dfrac{Wt}{p})
	\]

Our simulation showed that at first the \emph{Dynamo} policy distributes better
the data because the total size of the read and write quorums is greater
than in the \emph{Grid} case. But in the case of a large number of
operations the \emph{Grid} system will be less loaded than \emph{Dynamo}
because less nodes are used in operations.


\section{Conclusion}
This paper describes two quorum policies used in distributed computing
and underlines the importance of two metrics in achieving consistency,
load distribution and high availability. The rules that must be followed
to build a \emph{Grid} quorum system are more restrictive than in
\emph{Dynamo's} case making it more prone to consistency errors. On the
other hand, given the fact that the intersection of two read and write
quorums is exactly one node and that a \emph{Grid} quorum is made of $\sqrt{N}$
nodes helps distributing the load better in case of increased number of
operations.

We presented a Java application which implements the two quorum policies
and simulates different situations where read and write operations are
performed on a set of virtual nodes. The implementation focuses only on the
policy and collects availability statistics in scenarios where a part of
the nodes are failed.

Our work leaves a couple of open challenges and directions for future
work. Aside from the two metrics we have studied, one can also study the
\emph{capacity} of a quorum system (the highest quorum access rate that
a set of nodes can handle) to determine which policy allows the handling
of as many requests as possible. \cite{load} Another improvement of our
research would be to take into account different versions of the same
object stored in the system and make more accurate measurement with an
increased number of writes and reads.
\newline


\bibliographystyle{bibtex/IEEEtran}
\bibliography{bibtex/quorum.bib}

% that's all folks
\end{document}


