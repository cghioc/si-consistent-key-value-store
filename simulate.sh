#!/bin/bash
NODE_NUMBER=100
OBJECT_NUMBER=100
DYNAMO_CONFIG_FILE=dynamo.cfg
GRID_CONFIG_FILE=grid.cfg

dyn_reads=(50 40 30 60 70 20 80)
dyn_writes=(51 61 71 41 31 81 21)
grid_reads=(10 4 25 50 2)
grid_writes=(10 25 4 2 50)
dyn_case_number=7
grid_case_number=5

function simulate_dynamo {
	echo "Simulating with Dynamo"

	for (( i=0; i<$dyn_case_number; i++ )); do
		echo "Reads ${dyn_reads[i]}, writes ${dyn_writes[i]}"

		echo $NODE_NUMBER	> $DYNAMO_CONFIG_FILE
		echo $OBJECT_NUMBER	>> $DYNAMO_CONFIG_FILE
		echo ${dyn_reads[i]}	>> $DYNAMO_CONFIG_FILE
		echo ${dyn_writes[i]}	>> $DYNAMO_CONFIG_FILE

		echo ""
		ant dynamo | grep Imbalance
		gnuplot -e "filename='dynamo_out.dat'" -e \
			"fileout='output/dyn_${dyn_reads[i]}.${dyn_writes[i]}.png'" plot_dyn.plg
	done
}

function simulate_grid {
	echo "Simulating with Grid"

	for (( i=0; i<$grid_case_number; i++ )); do
		echo "Reads ${grid_reads[i]}, write ${grid_writes[i]}"

		echo $NODE_NUMBER	> $GRID_CONFIG_FILE
		echo $OBJECT_NUMBER	>> $GRID_CONFIG_FILE
		echo ${grid_reads[i]}	>> $GRID_CONFIG_FILE
		echo ${grid_writes[i]}	>> $GRID_CONFIG_FILE

		echo ""
		ant grid | grep Imbalance
		gnuplot -e "filename='grid_out.dat'" -e \
			"fileout='output/grid_${grid_reads[i]}.${grid_writes[i]}.png'" plot_grid.plg
	done
}


if [ "$#" != 1 ]; then
	echo "Incorrect number of parameters. How to run: ./simulate.sh dynamo/grid"
	exit
fi

mkdir output
ant build


if [ "$1" == "dynamo" ]; then
	simulate_dynamo
elif [ "$1" == "grid" ]; then
	simulate_grid
else
	echo "Incorrect parameter. Accepted values: dynamo or grid"
	exit
fi
